//
//  IntroScrollViewController.swift
//  GExpress
//
//  Created by Ann on 1/11/16.
//  Copyright © 2016 Unemployment. All rights reserved.
//

import UIKit

class IntroScrollViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("did load?")
        
        let firstChildViewContorller = childViewControllers.first
        scrollView.contentSize = CGSizeMake(CGRectGetWidth((firstChildViewContorller?.view.frame)!) * 4, CGRectGetHeight((firstChildViewContorller?.view.frame)!))
        print("frame \(firstChildViewContorller!.view.frame)")
        print("contentsize \(scrollView.contentSize)")
        print("sub\(firstChildViewContorller!.view.superview)")
        pageControl.currentPage = 0
        
    
        firstChildViewContorller?.didMoveToParentViewController(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        print("end")
        
        let pageWidth = CGRectGetWidth(scrollView.frame)
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2.0) / pageWidth) + 1.0)
        
        pageControl.currentPage = page
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        print("scrollViewdidscroll")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
